package log

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestLevel_String(t *testing.T) {
	Convey("日志等级测试", t, func() {
		lv := LevelDebug
		So(lv.String(), ShouldEqual, "debug")
	})
}
