package rollwriter

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"
	"sync"
	"testing"
	"time"

	"github.com/natefinch/lumberjack"
	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	rotatelogs "github.com/lestrrat-go/file-rotatelogs"
)

// 功能测试
// go test -v -cover
// 性能测试
// go test -bench=. -benchtime=20s -run=Bench

const (
	testTimes    = 100000
	testRoutines = 256
)

func TestRollWriter(t *testing.T) {
	// 清理上次遗留的测试文件
	logDir := "/tmp/log_test"
	os.RemoveAll(logDir)

	// 空文件名
	t.Run("empty_log_name", func(t *testing.T) {
		_, err := NewRollWriter("")
		assert.Error(t, err, "NewRollWriter: invalid log path")
	})

	// 不滚动
	t.Run("roll_by_default", func(t *testing.T) {
		logName := "test.log"
		w, err := NewRollWriter(filepath.Join(logDir, logName))
		assert.NoError(t, err, "NewRollWriter: create logger ok")
		log.SetOutput(w)
		for i := 0; i < testTimes; i++ {
			log.Printf("this is a test log: %d\n", i)
		}
		w.Close()

		time.Sleep(1 * time.Second)
		logFiles := getLogBackups(logDir, logName)
		if len(logFiles) != 1 {
			t.Errorf("Number of log backup files should be 1")
		}
	})

	// 按大小滚动
	t.Run("roll_by_size", func(t *testing.T) {
		logName := "test_size.log"
		w, err := NewRollWriter(filepath.Join(logDir, logName),
			WithMaxSize(1),
			WithMaxAge(1),
			WithMaxBackups(2),
		)
		assert.NoError(t, err, "NewRollWriter: create logger ok")
		log.SetOutput(w)
		for i := 0; i < testTimes; i++ {
			log.Printf("this is a test log: %d\n", i)
		}
		w.Close()

		// 检查滚动的日志文件个数(当前文件+备份文件)
		time.Sleep(1 * time.Second)
		logFiles := getLogBackups(logDir, logName)
		if len(logFiles) != 3 {
			t.Errorf("Number of log backup files should be 3")
		}

		// 检查滚动的日志文件大小(允许超出一点点)
		for _, file := range logFiles {
			if file.Size() > 1*1024*1024+1024 {
				t.Errorf("Log file size exceeds max_size")
			}
		}
	})

	// 按时间滚动
	t.Run("roll_by_time", func(t *testing.T) {
		logName := "test_time.log"
		w, err := NewRollWriter(filepath.Join(logDir, logName),
			WithRotationTime(".%Y%m%d"),
			WithMaxSize(1),
			WithMaxAge(1),
			WithMaxBackups(3),
			WithCompress(true),
		)
		assert.NoError(t, err, "NewRollWriter: create logger ok")
		log.SetOutput(w)
		for i := 0; i < testTimes; i++ {
			log.Printf("this is a test log: %d\n", i)
		}
		w.Close()

		// 检查滚动的日志文件个数
		time.Sleep(1 * time.Second)
		logFiles := getLogBackups(logDir, logName)
		if len(logFiles) != 4 {
			t.Errorf("Number of log files should be 4")
		}

		// 检查滚动的日志文件大小(允许超出一点点)
		for _, file := range logFiles {
			if file.Size() > 1*1024*1024+1024 {
				t.Errorf("Log file size exceeds max_size")
			}
		}

		// 检查压缩文件个数
		compressFileNum := 0
		for _, file := range logFiles {
			if strings.HasSuffix(file.Name(), compressSuffix) {
				compressFileNum++
			}
		}
		if compressFileNum != 3 {
			t.Errorf("Number of compress log files should be 3")
		}
	})

	// 等待1s
	time.Sleep(1 * time.Second)

	// 打印日志文件列表
	printLogFiles(logDir)
}

func TestAsyncRollWriter(t *testing.T) {
	// 清理上次遗留的测试文件
	logDir := "/tmp/log_async_test"
	os.RemoveAll(logDir)

	// 不滚动(异步模式)
	t.Run("roll_by_default_async", func(t *testing.T) {
		logName := "test.log"
		w, err := NewRollWriter(filepath.Join(logDir, logName))
		assert.NoError(t, err, "NewRollWriter: create logger ok")

		asyncWriter := NewAsyncRollWriter(w, WithLogQueueSize(10), WithWriteLogSize(1024),
			WithWriteLogInterval(100), WithDropLog(true))
		log.SetOutput(asyncWriter)
		for i := 0; i < testTimes; i++ {
			log.Printf("this is a test log: %d\n", i)
		}
		_ = asyncWriter.Close()

		time.Sleep(1 * time.Second)
		logFiles := getLogBackups(logDir, logName)
		if len(logFiles) != 1 {
			t.Errorf("Number of log backup files should be 1")
		}
	})

	// 按大小滚动(异步模式)
	t.Run("roll_by_size_async", func(t *testing.T) {
		logName := "test_size.log"
		w, err := NewRollWriter(filepath.Join(logDir, logName),
			WithMaxSize(1),
			WithMaxAge(1),
		)
		assert.NoError(t, err, "NewRollWriter: create logger ok")

		asyncWriter := NewAsyncRollWriter(w)
		log.SetOutput(asyncWriter)
		for i := 0; i < testTimes; i++ {
			log.Printf("this is a test log: %d\n", i)
		}
		_ = asyncWriter.Close()

		// 检查滚动的日志文件个数(当前文件+备份文件)
		time.Sleep(1 * time.Second)
		logFiles := getLogBackups(logDir, logName)
		if len(logFiles) != 5 {
			t.Errorf("Number of log backup files should be 5")
		}

		// 检查滚动的日志文件大小(异步模式, 最多可能会超出4k大小)
		for _, file := range logFiles {
			if file.Size() > 1*1024*1024+4*1024 {
				t.Errorf("Log file size exceeds max_size")
			}
		}
	})

	// 按时间滚动(异步模式)
	t.Run("roll_by_time_async", func(t *testing.T) {
		logName := "test_time.log"
		w, err := NewRollWriter(filepath.Join(logDir, logName),
			WithRotationTime(".%Y%m%d"),
			WithMaxSize(1),
			WithMaxAge(1),
			WithCompress(true),
		)
		assert.NoError(t, err, "NewRollWriter: create logger ok")

		asyncWriter := NewAsyncRollWriter(w)
		log.SetOutput(asyncWriter)
		for i := 0; i < testTimes; i++ {
			log.Printf("this is a test log: %d\n", i)
		}
		_ = asyncWriter.Close()

		// 检查滚动的日志文件个数
		time.Sleep(1 * time.Second)
		logFiles := getLogBackups(logDir, logName)
		if len(logFiles) != 5 {
			t.Errorf("Number of log files should be 4")
		}

		// 检查滚动的日志文件大小(异步模式, 最多可能会超出4k大小)
		for _, file := range logFiles {
			if file.Size() > 1*1024*1024+4*1024 {
				t.Errorf("Log file size exceeds max_size")
			}
		}

		// 检查压缩文件个数
		compressFileNum := 0
		for _, file := range logFiles {
			if strings.HasSuffix(file.Name(), compressSuffix) {
				compressFileNum++
			}
		}
		if compressFileNum != 4 {
			t.Errorf("Number of compress log files should be 3")
		}
	})

	// 等待1s
	time.Sleep(1 * time.Second)

	// 打印日志文件列表
	printLogFiles(logDir)
}

func TestRollWriterRace(t *testing.T) {
	// 清理上次遗留的测试文件
	logDir := "/tmp/log_bench/rollwriter_race"
	os.RemoveAll(logDir)

	writer, _ := NewRollWriter(
		filepath.Join(logDir, "test.log"),
		WithRotationTime(".%Y%m%d"),
	)
	writer.opts.MaxSize = 1

	wg := sync.WaitGroup{}
	wg.Add(2)
	go func() {
		defer wg.Done()
		for i := 0; i < 100; i++ {
			writer.Write([]byte(fmt.Sprintf("this is a test log: 1-%d\n", i)))
		}
	}()

	go func() {
		defer wg.Done()
		for i := 0; i < 100; i++ {
			writer.Write([]byte(fmt.Sprintf("this is a test log: 2-%d\n", i)))
		}
	}()
	wg.Wait()

	_ = writer.Close()
	execCommand("/bin/bash", "-c", "cat "+logDir+"/*|wc -l")
}

func TestAsyncRollWriterRace(t *testing.T) {
	// 清理上次遗留的测试文件
	logDir := "/tmp/log_bench/async_rollwriter_race"
	os.RemoveAll(logDir)

	writer, _ := NewRollWriter(
		filepath.Join(logDir, "test.log"),
		WithRotationTime(".%Y%m%d"),
	)
	writer.opts.MaxSize = 1
	w := NewAsyncRollWriter(writer)

	wg := sync.WaitGroup{}
	wg.Add(2)
	go func() {
		defer wg.Done()
		for i := 0; i < 100; i++ {
			w.Write([]byte(fmt.Sprintf("this is a test log: 1-%d\n", i)))
		}
	}()

	go func() {
		defer wg.Done()
		for i := 0; i < 100; i++ {
			w.Write([]byte(fmt.Sprintf("this is a test log: 2-%d\n", i)))
		}
	}()
	wg.Wait()

	_ = writer.Close()
	time.Sleep(1 * time.Second)
	execCommand("/bin/bash", "-c", "cat "+logDir+"/*|wc -l")
}

// BenchmarkRollWriterBySize benchmark rollwriter_bysize
func BenchmarkRollWriterBySize(b *testing.B) {
	// 清理上次遗留的测试文件
	logDir := "/tmp/log_bench/rollwriter_bysize"
	os.RemoveAll(logDir)

	// 初始化日志对象
	writer, _ := NewRollWriter(filepath.Join(logDir, "test.log"))
	core := zapcore.NewCore(
		zapcore.NewConsoleEncoder(zap.NewProductionEncoderConfig()),
		zapcore.AddSync(writer),
		zapcore.DebugLevel,
	)
	logger := zap.New(
		core,
		zap.AddCaller(),
	)

	// 预热
	for i := 0; i < testTimes; i++ {
		logger.Debug(fmt.Sprint("this is a test log: ", i))
	}

	b.SetParallelism(testRoutines / runtime.GOMAXPROCS(0))
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			logger.Debug("this is a test log")
		}
	})
}

// BenchmarkRollWriterByTime benchmark rollwriter_bytime
func BenchmarkRollWriterByTime(b *testing.B) {
	// 清理上次遗留的测试文件
	logDir := "/tmp/log_bench/rollwriter_bytime"
	os.RemoveAll(logDir)

	// 初始化日志对象
	writer, _ := NewRollWriter(
		filepath.Join(logDir, "test.log"),
		WithRotationTime(".%Y%m%d"),
	)
	core := zapcore.NewCore(
		zapcore.NewConsoleEncoder(zap.NewProductionEncoderConfig()),
		zapcore.AddSync(writer),
		zapcore.DebugLevel,
	)
	logger := zap.New(
		core,
		zap.AddCaller(),
	)

	// 预热
	for i := 0; i < testTimes; i++ {
		logger.Debug(fmt.Sprint("this is a test log: ", i))
	}

	b.SetParallelism(testRoutines / runtime.GOMAXPROCS(0))
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			logger.Debug("this is a test log")
		}
	})
}

// BenchmarkAsyncRollWriterBySize benchmark async rollwriter
func BenchmarkAsyncRollWriterBySize(b *testing.B) {
	// 清理上次遗留的测试文件
	logDir := "/tmp/log_bench/async_rollwriter_bysize"
	os.RemoveAll(logDir)

	// 初始化日志对象
	writer, _ := NewRollWriter(
		filepath.Join(logDir, "test.log"),
	)
	asyncWriter := NewAsyncRollWriter(writer)
	core := zapcore.NewCore(
		zapcore.NewConsoleEncoder(zap.NewProductionEncoderConfig()),
		asyncWriter,
		zapcore.DebugLevel,
	)
	logger := zap.New(
		core,
		zap.AddCaller(),
	)

	// 预热
	for i := 0; i < testTimes; i++ {
		logger.Debug(fmt.Sprint("this is a test log: ", i))
	}

	b.SetParallelism(testRoutines / runtime.GOMAXPROCS(0))
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			logger.Debug("this is a test log")
		}
	})
}

// BenchmarkAsyncRollWriterByTime benchmark async_rollwriter_bytime
func BenchmarkAsyncRollWriterByTime(b *testing.B) {
	// 清理上次遗留的测试文件
	logDir := "/tmp/log_bench/async_rollwriter_bytime"
	os.RemoveAll(logDir)

	// 初始化日志对象
	writer, _ := NewRollWriter(
		filepath.Join(logDir, "test.log"),
		WithRotationTime(".%Y%m%d"),
	)
	asyncWriter := NewAsyncRollWriter(writer)
	core := zapcore.NewCore(
		zapcore.NewConsoleEncoder(zap.NewProductionEncoderConfig()),
		asyncWriter,
		zapcore.DebugLevel,
	)
	logger := zap.New(
		core,
		zap.AddCaller(),
	)

	// 预热
	for i := 0; i < testTimes; i++ {
		logger.Debug(fmt.Sprint("this is a test log: ", i))
	}

	b.SetParallelism(testRoutines / runtime.GOMAXPROCS(0))
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			logger.Debug("this is a test log")
		}
	})
}

// BenchmarkLumberjack benchmark lumberjack
func BenchmarkLumberjack(b *testing.B) {
	// 清理上次遗留的测试文件
	logDir := "/tmp/log_bench/lumberjack"
	os.RemoveAll(logDir)

	// 初始化日志对象
	writer := &lumberjack.Logger{
		Filename:   filepath.Join(logDir, "test.log"),
		MaxSize:    100 * 1024 * 1024,
		MaxBackups: 0,
		MaxAge:     0,
		Compress:   false,
	}
	core := zapcore.NewCore(
		zapcore.NewConsoleEncoder(zap.NewProductionEncoderConfig()),
		zapcore.AddSync(writer),
		zapcore.DebugLevel,
	)
	logger := zap.New(
		core,
		zap.AddCaller(),
	)
	// 预热
	for i := 0; i < testTimes; i++ {
		logger.Debug(fmt.Sprint("this is a test log: ", i))
	}

	b.SetParallelism(testRoutines / runtime.GOMAXPROCS(0))
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			logger.Debug("this is a test log")
		}
	})
}

// BenchmarkRotatelogs benchmark rotatelogs
func BenchmarkRotatelogs(b *testing.B) {
	// 清理上次遗留的测试文件
	logDir := "/tmp/log_bench/rotatelogs"
	os.RemoveAll(logDir)

	// 初始化日志对象
	writer, _ := rotatelogs.New(
		filepath.Join(logDir, "test.log")+".%Y%m%d",
		rotatelogs.WithMaxAge(0),
		rotatelogs.WithRotationTime(24*time.Hour),
	)
	core := zapcore.NewCore(
		zapcore.NewConsoleEncoder(zap.NewProductionEncoderConfig()),
		zapcore.AddSync(writer),
		zapcore.DebugLevel,
	)
	logger := zap.New(
		core,
		zap.AddCaller(),
	)
	// 预热
	for i := 0; i < testTimes; i++ {
		logger.Debug(fmt.Sprint("this is a test log: ", i))
	}

	b.SetParallelism(testRoutines / runtime.GOMAXPROCS(0))
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			logger.Debug("this is a test log")
		}
	})
}

func printLogFiles(logDir string) {
	fmt.Println("================================================")
	fmt.Printf("[%s]:\n", logDir)
	files, err := ioutil.ReadDir(logDir)
	if err != nil {
		fmt.Println("ReadDir failed ", err)
		return
	}
	for _, file := range files {
		fmt.Println("\t", file.Name(), file.Size())
	}
}

func execCommand(name string, args ...string) {
	fmt.Println(name, args)
	cmd := exec.Command(name, args...)
	output, err := cmd.Output()
	if err != nil {
		fmt.Printf("exec command failed, err: %v\n", err)
	}
	fmt.Println(string(output))
}

func getLogBackups(logDir string, prefix string) []os.FileInfo {
	logFiles := []os.FileInfo{}
	files, err := ioutil.ReadDir(logDir)
	if err != nil {
		return logFiles
	}

	for _, file := range files {
		if !strings.HasPrefix(file.Name(), prefix) {
			continue
		}

		logFiles = append(logFiles, file)
	}

	return logFiles
}
