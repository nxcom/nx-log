// Package log 提供框架和应用日志输出能力
package log

import (
	"fmt"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func init() {
	//SetLogger(NewZapLog(defaultConfig))
}

var traceEnabled = false

// EnableTrace 开启trace级别日志
func EnableTrace() {
	traceEnabled = true
}

// DefaultLogger 默认的logger，通过配置文件key=default来设置, 默认使用console输出
var DefaultLogger Logger

// SetLogger 设置默认logger
func SetLogger(logger Logger) {
	DefaultLogger = logger
}

// SetLevel 设置不同的输出对应的日志级别, output为输出数组下标 "0" "1" "2"
func SetLevel(output string, level Level) {
	DefaultLogger.SetLevel(output, level)
}

// GetLevel 获取不同输出对应的日志级别
func GetLevel(output string) Level {
	return DefaultLogger.GetLevel(output)
}

// WithFields 设置一些业务自定义数据到每条log里:比如uid，imei等, fields 必须kv成对出现
func WithFields(fields ...string) Logger {
	return DefaultLogger.WithFields(fields...)
}


// RedirectStdLog 重定向标准库 log 的输出到 logger 的 info 级别日志中
// 重定向后，log flag 为 0，prefix 为空
// 它返回的函数可以用来恢复 log flag 和 prefix，并将输出重定向到 os.Stderr
func RedirectStdLog(logger Logger) (func(), error) {
	return RedirectStdLogAt(logger, zap.InfoLevel)
}

// RedirectStdLogAt 重定向标准库 log 的输出到 logger 指定级别的日志中
// 重定向后，log flag 为 0，prefix 为空
// 它返回的函数可以用来恢复 log flag 和 prefix，并将输出重定向到 os.Stderr
func RedirectStdLogAt(logger Logger, level zapcore.Level) (func(), error) {
	if l, ok := logger.(*zapLog); ok {
		return zap.RedirectStdLogAt(l.logger, level)
	}
	if l, ok := logger.(*ZapLogWrapper); ok {
		return zap.RedirectStdLogAt(l.l.logger, level)
	}
	return nil, fmt.Errorf("log: only supports redirecting std logs to zap logger")
}

// Trace logs to TRACE log. Arguments are handled in the manner of fmt.Print.
func Trace(args ...interface{}) {
	if traceEnabled {
		DefaultLogger.Trace(args...)
	}
}

// Tracef logs to TRACE log. Arguments are handled in the manner of fmt.Printf.
func Tracef(format string, args ...interface{}) {
	if traceEnabled {
		DefaultLogger.Tracef(format, args...)
	}
}


// Debug logs to DEBUG log. Arguments are handled in the manner of fmt.Print.
func Debug(args ...interface{}) {
	DefaultLogger.Debug(args...)
}

// Debugf logs to DEBUG log. Arguments are handled in the manner of fmt.Printf.
func Debugf(format string, args ...interface{}) {
	DefaultLogger.Debugf(format, args...)
}

// Info logs to INFO log. Arguments are handled in the manner of fmt.Print.
func Info(args ...interface{}) {
	DefaultLogger.Info(args...)
}

// Infof logs to INFO log. Arguments are handled in the manner of fmt.Printf.
func Infof(format string, args ...interface{}) {
	DefaultLogger.Infof(format, args...)
}

// Warn logs to WARNING log. Arguments are handled in the manner of fmt.Print.
func Warn(args ...interface{}) {
	DefaultLogger.Warn(args...)
}

// Warnf logs to WARNING log. Arguments are handled in the manner of fmt.Printf.
func Warnf(format string, args ...interface{}) {
	DefaultLogger.Warnf(format, args...)
}

// Error logs to ERROR log. Arguments are handled in the manner of fmt.Print.
func Error(args ...interface{}) {
	DefaultLogger.Error(args...)
}

// Errorf logs to ERROR log. Arguments are handled in the manner of fmt.Printf.
func Errorf(format string, args ...interface{}) {
	DefaultLogger.Errorf(format, args...)
}

// Fatal logs to ERROR log. Arguments are handled in the manner of fmt.Print.
// that all Fatal logs will exit with os.Exit(1).
// Implementations may also call os.Exit() with a non-zero exit code.
func Fatal(args ...interface{}) {
	DefaultLogger.Fatal(args...)
}

// Fatalf logs to ERROR log. Arguments are handled in the manner of fmt.Printf.
func Fatalf(format string, args ...interface{}) {
	DefaultLogger.Fatalf(format, args...)
}

