package log_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitee.com/nxcom/nx-log"
)

var defaultConfig = []log.OutputConfig{
	{
		Writer:    "console",
		Level:     "debug",
		Formatter: "console",
		FormatConfig: log.FormatConfig{
			TimeFmt: "2006.01.02 15:04:05",
		},
	},
	{
		Writer:    "file",
		Level:     "info",
		Formatter: "json",
		WriteConfig: log.WriteConfig{
			Filename:   "nxcom_size.log",
			RollType:   "size",
			MaxAge:     7,
			MaxBackups: 10,
			MaxSize:    100,
		},
		FormatConfig: log.FormatConfig{
			TimeFmt: "2006.01.02 15:04:05",
		},
	},
	{
		Writer:    "file",
		Level:     "info",
		Formatter: "json",
		WriteConfig: log.WriteConfig{
			Filename:   "nxcom_time.log",
			RollType:   "time",
			MaxAge:     7,
			MaxBackups: 10,
			MaxSize:    100,
			TimeUnit:   log.Day,
		},
		FormatConfig: log.FormatConfig{
			TimeFmt: "2006-01-02 15:04:05",
		},
	},
}

func TestNewZapLog(t *testing.T) {
	logger := log.NewZapLog(defaultConfig)
	assert.NotNil(t, logger)

	logger.SetLevel("0", log.LevelInfo)
	lvl := logger.GetLevel("0")
	assert.Equal(t, lvl, log.LevelInfo)

	l := logger.WithFields("test", "a")
	if tmp, ok := l.(*log.ZapLogWrapper); ok {
		tmp.GetLogger()
		tmp.Sync()
	}
	l.SetLevel("output", log.LevelDebug)
	assert.Equal(t, log.LevelDebug, l.GetLevel("output"))
}

func BenchmarkDefaultTimeFormat(b *testing.B) {
	t := time.Now()
	for i := 0; i < b.N; i++ {
		log.DefaultTimeFormat(t)
	}
}

func BenchmarkCustomTimeFormat(b *testing.B) {
	t := time.Now()
	for i := 0; i < b.N; i++ {
		log.CustomTimeFormat(t, "2006-01-02 15:04:05.000")
	}
}

func TestCustomTimeFormat(t *testing.T) {
	date := time.Date(2006, 1, 2, 15, 4, 5, 0, time.Local)
	dateStr := log.CustomTimeFormat(date, "2006-01-02 15:04:05.000")
	assert.Equal(t, dateStr, "2006-01-02 15:04:05.000")
}

func TestDefaultTimeFormat(t *testing.T) {
	date := time.Date(2006, 1, 2, 15, 4, 5, 0, time.Local)
	dateStr := string(log.DefaultTimeFormat(date))
	assert.Equal(t, dateStr, "2006-01-02 15:04:05.000")
}

func TestGetLogEncoderKey(t *testing.T) {
	tests := []struct {
		name   string
		defKey string
		key    string
		want   string
	}{
		{"custom", "T", "Time", "Time"},
		{"default", "T", "", "T"},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := log.GetLogEncoderKey(tt.defKey, tt.key); got != tt.want {
				assert.Equal(t, got, tt.want)
			}
		})
	}
}

func TestNewTimeEncoder(t *testing.T) {
	encoder := log.NewTimeEncoder("")
	assert.NotNil(t, encoder)

	encoder = log.NewTimeEncoder("2006-01-02 15:04:05")
	assert.NotNil(t, encoder)

	tests := []struct {
		name string
		fmt  string
	}{
		{"seconds timestamp", "seconds"},
		{"milliseconds timestamp", "milliseconds"},
		{"nanoseconds timestamp", "nanoseconds"},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := log.NewTimeEncoder(tt.fmt)
			assert.NotNil(t, got)
		})
	}
}
